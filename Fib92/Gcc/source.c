/*
%d - int (same as %i)
%ld - long int (same as %li)
%f - float
%lf - double
%c - char
%s - string
%x - hexadecimal
*/
#include <stdio.h>

unsigned long long int fib(int n)
{
  unsigned long long int a = 1;
  unsigned long long int b = 1;
  unsigned long long int temp = 0;

    while (n > 0)
    {
      temp = b;
      b += a;
      a = temp;
      --n;
    }
    return a;
}

int main(int argc, char const* argv[])
{
  unsigned long long int r;

//Fib(93) = 12200160415121876738 (20 digits)//maximux for c
  r = fib(92);
  printf("R = %llu", r);

  return 0;
}
